using System;
using UnityEngine;

namespace Sfx
{
    public class SfxManager : MonoBehaviour
    {
        public static SfxManager Instance { get; private set; }

        private AudioSource sfx;

        private void Awake()
        {
            Instance = this;
            sfx = GetComponent<AudioSource>();
        }

        public void PlaySfx(string name)
        {
            var path = "sfx/" + name;
            var clip = Resources.Load<AudioClip>(path);
            sfx.PlayOneShot(clip);
        }
    }
}