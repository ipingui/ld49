using System;
using Player;
using TMPro;
using Tower;
using UnityEngine;

namespace Ui
{
    public class GeneralUi : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI boxesAmountText;
        [SerializeField] private TextMeshProUGUI stabilityText;

        private PlayerStatus playerStatus;
        private TowerManager towerManager;

        private void Awake()
        {
            playerStatus = FindObjectOfType<PlayerStatus>();
            towerManager = FindObjectOfType<TowerManager>();
        }

        private void Update()
        {
            boxesAmountText.text = playerStatus.BoxesAvailableToPlace.ToString();

            var percentage = Decimal.Round((decimal)towerManager.GetTowerStability()* 100, 2);
            stabilityText.text = percentage + "%";
        }
    }
}