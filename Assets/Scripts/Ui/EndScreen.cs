using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Ui
{
    public class EndScreen : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI resultText;
        
        public void TryAgain()
        {
            Debug.Log("E");
            SceneManager.LoadScene("MainGame");
        }

        public void SetResultText(int builds)
        {
            resultText.text = $"You built {builds} times! Try again for a higher score!";
        }
    }
}