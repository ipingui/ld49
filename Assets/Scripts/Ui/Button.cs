namespace UI
{
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;
    
    public class Button : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public bool Selected { get; set; }
        private Image image;
        private Color originalColor;
        public UnityEvent onClick;
    
        protected virtual void Awake()
        {
            image = GetComponent<Image>();
            originalColor = image.color;
        }
    
        protected virtual void Update()
        {
            if (Selected && Input.GetButtonDown("Submit"))
            {
                Selected = false;
                onClick.Invoke();
            }
        }
    
        public void OnPointerEnter(PointerEventData eventData)
        {
            Select();
        }
    
        public void OnPointerExit(PointerEventData eventData)
        {
            Deselect();
        }
    
        public void Select()
        {
            Selected = true;
            image.color = Color.grey;
        }
    
        public void Deselect()
        {
            Selected = false;
            image.color = originalColor;
        }
    }
}
