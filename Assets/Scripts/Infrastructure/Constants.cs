using UnityEngine;

namespace Infrastructure
{
    public static class Constants
    {
        public static readonly Vector2 BoxDimensions = new Vector2(9f, 7.5f);
    }
}