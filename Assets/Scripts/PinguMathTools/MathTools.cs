using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

namespace PinguMathTools
{
    public static class MathTools
    {
        private static Random rand = new Random();
    
        public static bool RandomBool()
        {
            var val = rand.Next(2);
            return val == 1;
        }

        public static T RandomChoice<T>(List<T> ul)
        {
            if (ul.Count == 0)
            {
                Debug.Log("Error: List is empty.");
            }
            var idx = rand.Next(ul.Count);
            return ul[idx];
        }
    
        public static List<T> RandomChoice<T>(List<T> ul, int numberOfChoices)
        {
            var choices = new List<T>();
            for (int i = 0; i < numberOfChoices; i++)
            {
                var choice = RandomChoice(ul);
                if (!choices.Contains(choice))
                {
                    choices.Add(choice);
                }
            }

            return choices;
        }

        public static int RandomInt(int min, int max)
        {
            return rand.Next(min, max);
        }
    
        public static int RandomInt(int max)
        {
            return rand.Next(max);
        }

        public static float RandomFloat()
        {
            return (float)rand.NextDouble();
        }

        public static List<T> EnumToList<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToList();
        }
    
        public static List<string> EnumToStringList<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().Select(e => e.ToString()).ToList();
        }
    
        public static T ParseEnum<T>(string value)
        {
            return (T) Enum.Parse(typeof(T), value, true);
        }

        public static double RandomGaussian(float mean=0.5f, float stdDev=0.3f)
        {
            var u1 = 1.0-rand.NextDouble(); //uniform(0,1] random doubles
            var u2 = 1.0-rand.NextDouble();
            var randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                                Math.Sin(2.0 * Math.PI * u2); //random normal(0,1)
            var randNormal =
                mean + stdDev * randStdNormal; //random normal(mean,stdDev^2)
        
            return randNormal;
        }

        public static int RandomGaussianInRange(int max)
        {
            var gaussian = RandomGaussian();
            var value = gaussian * max;
            return (int)Math.Round(value, 0);
        }
    
        public static int RandomGaussianInRange(int min, int max, float mean=0.5f, float stdDev=0.3f)
        {
            var gaussian = RandomGaussian(mean, stdDev);
            var diff = max - min;
            var value = min + gaussian * diff;
            return (int)Math.Round(value, 0);
        }
    
        public static void Shuffle<T>(this IList<T> list)  
        {  
            int n = list.Count;  
            while (n > 1) {  
                n--;  
                int k = rand.Next(n + 1);  
                T value = list[k];  
                list[k] = list[n];  
                list[n] = value;  
            }  
        }

        public static bool CheckLineStartsAndEndsOutsideOfBounds(this Rect rect, Vector2 startPoint, Vector2 endPoint)
        {
            var startsAndEndsOutsideOfRect = !rect.Contains(startPoint) && !rect.Contains(endPoint);
            return startsAndEndsOutsideOfRect;
        }
    
        public static bool LineLineIntersection(out Vector3 intersection, Vector3 line1StartPos, Vector3 line1Direction, Vector3 line2StartPos, Vector3 line2Direction)
        {
            var lineVec3 = line2StartPos - line1StartPos;
            var crossVec1And2 = Vector3.Cross(line1Direction, line2Direction);
            var crossVec3And2 = Vector3.Cross(lineVec3, line2Direction);
 
            var planarFactor = Vector3.Dot(lineVec3, crossVec1And2);
 
            //is coplanar, and not parrallel
            if (Mathf.Abs(planarFactor) < 0.0001f && crossVec1And2.sqrMagnitude > 0.0001f)
            {
                var s = Vector3.Dot(crossVec3And2, crossVec1And2) / crossVec1And2.sqrMagnitude;
                intersection = line1StartPos + (line1Direction * s);
                return true;
            }
            else
            {
                intersection = Vector3.zero;
                return false;
            }
        }

        public static decimal ConvertToDecimalPlaces(float val, int decimalPlaces)
        {
            return Decimal.Round((decimal)val, decimalPlaces);
        }

        public static MathToolsTransform RotateAround(Vector3 originalPosition, Quaternion originalRotation, Vector3 center, Vector3 axis, float angle)
        {
            var desiredRotation = Quaternion.AngleAxis(angle, axis);
            var directionRelativeToCentre = originalPosition - center;
            directionRelativeToCentre = desiredRotation * directionRelativeToCentre;
        
            var resultantPosition = center + directionRelativeToCentre;
            var resultantRotation = originalRotation * desiredRotation;
        
            var result = new MathToolsTransform(resultantPosition, resultantRotation);
            return result;
        }
    }
}
