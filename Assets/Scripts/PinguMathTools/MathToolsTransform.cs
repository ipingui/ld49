using UnityEngine;

namespace PinguMathTools
{
    public class MathToolsTransform
    {
        public Vector3 Position { get; set; }
        public Quaternion Rotation { get; set; }

        public MathToolsTransform(Vector3 position, Quaternion quaternion)
        {
            Position = position;
            Rotation = quaternion;
        }
    }
}
