using System;
using Tower;
using UnityEngine;

namespace Camera
{
    public class CameraController : MonoBehaviour
    {
        private GameplayCameraMovement gameplayCameraMovement;
        private TowerPlacementCursorUi _towerPlacementCursorUi;

        private void Awake()
        {
            gameplayCameraMovement = FindObjectOfType<GameplayCameraMovement>();
            _towerPlacementCursorUi = FindObjectOfType<TowerPlacementCursorUi>();
        }

        public void ActivateBoxPlacementCamera()
        {
            gameplayCameraMovement.SetActiveTransformToCursor();
            _towerPlacementCursorUi.RenderNextTransparentBox();
        }

        public void ActivateGameplayCamera()
        {
            gameplayCameraMovement.SetActiveTransformToPlayer();
            _towerPlacementCursorUi.StopRendering();
        }
    }
}