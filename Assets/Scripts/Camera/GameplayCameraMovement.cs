using System.Collections;
using Tower;

namespace Camera
{
    using System;
    using Player;
    using UnityEngine;
    
    [RequireComponent(typeof(Camera))]
    public class GameplayCameraMovement : MonoBehaviour
    {
        public Camera Cam { get; private set; }
        
        private Transform _playerTransform;
        private Transform _cursorTransform;
        private Transform _activeTransform;

        private const float MaxDistanceFromPlayer = 40f;
        private const float MinDistanceFromPlayer = 30f;
        private const float MovementSpeed = 0.3f;
        private const float DefaultZoom = 7f;
        
        private float currentDistanceFromPlayer;

        private Coroutine zoomCoroutine;

        private void Awake()
        {
            _playerTransform = FindObjectOfType<PlayerMovement>().transform;
            _cursorTransform = FindObjectOfType<TowerPlacementCursor>().transform;
            Cam = GetComponent<Camera>();
            Cam.orthographicSize = DefaultZoom;
        }

        private void Start()
        {
            SetActiveTransformToPlayer();
            SetDistanceToPlayer(MaxDistanceFromPlayer);
        }

        public void SetActiveTransformToPlayer()
        {
            _activeTransform = _playerTransform;
            
            if (zoomCoroutine != null)
            {
                StopCoroutine(zoomCoroutine);
            }
            
            zoomCoroutine = StartCoroutine(ChangeZoomLevel(DefaultZoom));
        }

        public void SetActiveTransformToCursor()
        {
            _activeTransform = _cursorTransform;

            if (zoomCoroutine != null)
            {
                StopCoroutine(zoomCoroutine);
            }
            
            zoomCoroutine = StartCoroutine(ChangeZoomLevel(12f));
        }

        public void Shake()
        {
            StartCoroutine(Shake(40));
        }

        private void FixedUpdate()
        {
            Follow();
        }

        private void Follow()
        {
            var targetPosition = GetPositionAtDistanceFromPlayer(currentDistanceFromPlayer);

            transform.position = Vector3.Lerp(transform.position, targetPosition, MovementSpeed);
        }

        private void SetDistanceToPlayer(float distance)
        {
            transform.position = GetPositionAtDistanceFromPlayer(distance);
            currentDistanceFromPlayer = distance;
        }

        private Vector3 GetPositionAtDistanceFromPlayer(float distance)
        {
            distance = Math.Max(MinDistanceFromPlayer, distance);
            distance = Math.Min(MaxDistanceFromPlayer, distance);
            
            var cameraToPlayerVector = transform.forward.normalized;
            return _activeTransform.position - cameraToPlayerVector * distance;
        }

        private IEnumerator ChangeZoomLevel(float newZoom)
        {
            const float duration = 1f;
            var timeElapsed = 0f;

            var startingOrthographicSize = Cam.orthographicSize;

            while (timeElapsed < duration)
            {
                timeElapsed += Time.deltaTime;
                Cam.orthographicSize = Mathf.SmoothStep(startingOrthographicSize, newZoom, duration);
                yield return null;
            }
        }
        
        IEnumerator Shake(int frames)
        {
            var frame = 0;
            var originalPosition = transform.localPosition;
            var shakeMagnitude = 0.25f;

            while (frame < frames)
            {
                var xOffset = UnityEngine.Random.Range(0f, 1f) * shakeMagnitude * 2f - shakeMagnitude;
                var yOffset = UnityEngine.Random.Range(0f, 1f) * shakeMagnitude * 2f - shakeMagnitude;
                transform.localPosition += new Vector3(xOffset, yOffset, 0);
                frame += 1;
                yield return null;
            }

            transform.localPosition = originalPosition;
        }
    }
}