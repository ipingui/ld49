using System;
using Camera;
using Infrastructure;
using Player;
using Sfx;
using UnityEngine;

namespace Tower
{
    public class TowerPlacementCursor : MonoBehaviour
    {
        private Vector2Int currentGridPosition = new Vector2Int(0, 0);
        private TowerBoxQueue towerBoxQueue;
        private TowerManager towerManager;
        private TowerPlacementCursorUi towerPlacementCursorUi;
        private PlayerStatus playerStatus;
        private GameplayCameraMovement gameplayCameraMovement;

        private void Awake()
        {
            towerBoxQueue = FindObjectOfType<TowerBoxQueue>();
            towerManager = FindObjectOfType<TowerManager>();
            towerPlacementCursorUi = GetComponent<TowerPlacementCursorUi>();
            playerStatus = FindObjectOfType<PlayerStatus>();
            gameplayCameraMovement = FindObjectOfType<GameplayCameraMovement>();
        }

        private void Update()
        {
            transform.localPosition = GridCoordsToWorldCoords();
        }

        public void MoveUp()
        {
            currentGridPosition.y += 1;
        }

        public void MoveLeft()
        {
            currentGridPosition.x -= 1;
        }

        public void MoveRight()
        {
            currentGridPosition.x += 1;
        }

        public void MoveDown()
        {
            currentGridPosition.y -= 1;
        }

        public void PlaceNextBox()
        {
            if (!towerManager.CanPlaceHere(currentGridPosition))
            {
                SfxManager.Instance.PlaySfx("negative");
                return;
            }
            
            var playerHasEnoughBoxes = playerStatus.UseBox();
            if (!playerHasEnoughBoxes)
            {
                SfxManager.Instance.PlaySfx("negative");
                return;
            }

            var box = towerBoxQueue.GetNextBox();
            towerManager.SpawnBox(box, currentGridPosition);
            towerPlacementCursorUi.RenderNextTransparentBox();
            SfxManager.Instance.PlaySfx("place");
            gameplayCameraMovement.Shake();
        }

        private Vector2 GridCoordsToWorldCoords()
        {
            var origin = new Vector2(0, 0);
            var newX = origin.x + Constants.BoxDimensions.x * currentGridPosition.x;
            var newY = origin.y + Constants.BoxDimensions.y * currentGridPosition.y;
            return new Vector2(newX, newY);
        }
    }
}