using System;
using Sfx;
using TMPro;
using UnityEngine;

namespace Tower
{
    public class TowerBoxUi : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI amountText;
        [SerializeField] private TextMeshProUGUI infoText;
        [SerializeField] private TweenAlpha tweenAlpha;
        [SerializeField] private TweenTransforms tweenTransforms;

        private bool animationTriggered;

        public void SetAmount(int amount)
        {
            amountText.text = amount.ToString();
            infoText.text = $"Build {amount} times to unlock this area.";
        }

        public void UnlockAnimation()
        {
            if (animationTriggered)
            {
                return;
            }
            
            SfxManager.Instance.PlaySfx("unlock");
            animationTriggered = true;
            tweenTransforms.Stop();
            tweenAlpha.Begin();
            amountText.enabled = false;
            infoText.enabled = false;
        }
    }
}