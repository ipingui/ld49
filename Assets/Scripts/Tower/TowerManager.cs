using System;
using System.Collections.Generic;
using System.Linq;
using Camera;
using Infrastructure;
using Player;
using Sfx;
using Ui;
using UnityEngine;

namespace Tower
{
    public class TowerManager : MonoBehaviour
    {
        private HashSet<Vector2Int> populatedGridPositions;
        private int lastBoxSpawnX;

        private float tilt;

        [SerializeField] private GameObject background;
        public int PlacedBoxes { get; private set; }

        private bool initialising;
        private bool gameOver;
        private const float MaxTilt = 20f;
        
        private void Awake()
        {
            initialising = true;
            
            populatedGridPositions = new HashSet<Vector2Int>();
            
            // The pre-existing positions.
            populatedGridPositions.Add(new Vector2Int(0, -1));
            populatedGridPositions.Add(new Vector2Int(0, 0));
            populatedGridPositions.Add(new Vector2Int(0, 1));

            const int startCentreTowerAtY = 2;

            var lockedPrefab = Resources.Load<TowerBox>("prefabs/LockedTowerBox");
            var disabledPrefab = Resources.Load<TowerBox>("prefabs/DisabledTowerBox");

            for (int i = 0; i < 100; i++)
            {
                var yPosition = i + startCentreTowerAtY;
                var centreTowerCoord = new Vector2Int(0, yPosition);
                
                var locked = i % 3 == 0;
                var prefab = locked ? lockedPrefab : disabledPrefab;

                var spawnedBox = SpawnBox(prefab, centreTowerCoord);
                if (locked)
                {
                    var amountToOpen = ((i + 1) * 8) / 3;
                    spawnedBox.SetUnlockPrice(amountToOpen);
                }
            }

            initialising = false;
        }

        private void Update()
        {
            if (gameOver)
            {
                return;
            }
            
            if (Math.Abs(tilt) >= MaxTilt)
            {
                // wipe the screeen and say u lose
                tilt = MaxTilt;
                SpawnEndScreen();
                return;
            }
            
            tilt += lastBoxSpawnX * Time.deltaTime;

            background.transform.rotation = Quaternion.AngleAxis(tilt, Vector3.forward);
        }

        private void SpawnEndScreen()
        {
            var endScreenPrefab = Resources.Load<EndScreen>("prefabs/EndScreen");
            var endScreen = Instantiate(endScreenPrefab);
            var player = FindObjectOfType<PlayerInputHandler>();
            player.HasControl = false;
            endScreen.SetResultText(PlacedBoxes);
            SfxManager.Instance.PlaySfx("lose");
            gameOver = true;
        }

        public bool CanPlaceHere(Vector2Int gridPosition)
        {
            if (GridPositionContainsBox(gridPosition))
            {
                return false;
            }

            return AdjacentPositionExists(gridPosition);
        }

        public TowerBox SpawnBox(TowerBox towerBox, Vector2Int gridPosition)
        {
            var instantiatedBox = Instantiate(towerBox, transform);
            instantiatedBox.transform.localPosition = GridCoordsToWorldCoords(gridPosition);
            populatedGridPositions.Add(gridPosition);

            lastBoxSpawnX = gridPosition.x;

            if (!initialising)
            {
                PlacedBoxes += 1;
            }

            instantiatedBox.TowerManager = this;
            return instantiatedBox;
        }

        public float GetTowerStability()
        {
            return Math.Abs(tilt / MaxTilt);
        }
        
        private bool GridPositionContainsBox(Vector2Int gridPosition)
        {
            return populatedGridPositions.Contains(gridPosition);
        }

        private bool AdjacentPositionExists(Vector2Int gridPosition)
        {
            return populatedGridPositions.Any(pos =>
                Adjacent(pos.x, gridPosition.x) && Adjacent(pos.y, gridPosition.y));
        }

        private static bool Adjacent(int val1, int val2)
        {
            var diff = Math.Abs(val2 - val1);
            return diff <= 1;
        }
        
        private Vector2 GridCoordsToWorldCoords(Vector2Int gridPosition)
        {
            var origin = new Vector2(0, 0);
            var newX = origin.x + Constants.BoxDimensions.x * gridPosition.x;
            var newY = origin.y + Constants.BoxDimensions.y * gridPosition.y;
            return new Vector2(newX, newY);
        }
    }
}