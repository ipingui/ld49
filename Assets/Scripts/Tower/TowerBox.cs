using System;
using Infrastructure;
using PinguMathTools;
using Player;
using Player.Pickups;
using UnityEngine;

namespace Tower
{
    public class TowerBox : MonoBehaviour
    {
        public bool Locked { get; private set; }
        public int UnlockPrice { get; private set; }

        [SerializeField] private TowerBoxUi towerBoxUi;
        [SerializeField] private bool disabled;

        private bool transparent;
        private BoxCollider2D boxCollider2D;
        
        public TowerManager TowerManager { get; set; }

        private void Awake()
        {
            boxCollider2D = GetComponent<BoxCollider2D>();
        }

        private void Start()
        {
            if (!Locked && !disabled && !transparent)
            {
                SpawnBoxPickup();
            }
        }

        public void SetUnlockPrice(int price)
        {
            Locked = true;
            UnlockPrice = price;
            towerBoxUi.SetAmount(price);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!Locked)
            {
                return;
            }
            
            var player = other.gameObject.GetComponent<PlayerStatus>();
            if (player && TowerManager.PlacedBoxes >= UnlockPrice)
            {
                Unlock();
            }
        }

        private void Unlock()
        {
            towerBoxUi.UnlockAnimation();
            boxCollider2D.isTrigger = true;
        }

        public void SetTransparent()
        {
            transparent = true;
            var childSprites = GetComponentsInChildren<SpriteRenderer>();
            foreach (var childSprite in childSprites)
            {
                var c = childSprite.color;
                childSprite.color = new Color(c.r, c.g, c.b, 0.35f);
                var boxCollider = childSprite.gameObject.GetComponent<BoxCollider2D>();
                boxCollider.enabled = false;
                var rigidBody = childSprite.gameObject.GetComponent<Rigidbody2D>();
                if (rigidBody)
                {
                    rigidBody.bodyType = RigidbodyType2D.Static;
                }
            }
        }

        private void SpawnBoxPickup()
        {
            var xDirection = MathTools.RandomBool() ? 1 : -1;
            var randomX = MathTools.RandomFloat() * 0.4f * xDirection;
            
            var yDirection = MathTools.RandomBool() ? 1 : -1;
            var randomY = MathTools.RandomFloat() * 0.4f * yDirection;

            var boxPrefab = Resources.Load<BoxPickup>("prefabs/BoxPickup");
            var box = Instantiate(boxPrefab, transform);
            box.transform.localPosition = new Vector3(randomX, randomY, 0);
        }
    }
}