using System;
using System.Collections.Generic;
using PinguMathTools;
using UnityEngine;

namespace Tower
{
    public class TowerBoxQueue : MonoBehaviour
    {
        private const int NumberOfBoxPrefabs = 10;
        private List<TowerBox> boxes;

        private void Awake()
        {
            boxes = new List<TowerBox>();
            PrePopulateBoxQueue();
        }

        public TowerBox PreviewNextBox()
        {
            return boxes[0];
        }
        
        public TowerBox GetNextBox()
        {
            var box = boxes[0];
            boxes.RemoveAt(0);

            var replacementBoxForQueue = GetRandomBox();
            boxes.Add(replacementBoxForQueue);

            return box;
        }

        private void PrePopulateBoxQueue()
        {
            for (int i = 0; i < 10; i++)
            {
                boxes.Add(GetRandomBox());
            }
        }

        private static TowerBox GetRandomBox()
        {
            var number = MathTools.RandomInt(0, NumberOfBoxPrefabs);
            var prefab = Resources.Load<TowerBox>($"prefabs/TowerBox{number}");
            return prefab;
        }
    }
}