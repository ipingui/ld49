using System;
using UnityEngine;

namespace Tower
{
    public class TowerPlacementCursorUi : MonoBehaviour
    {
        private TowerBox currentTransparentBox;
        private TowerPlacementCursor towerPlacementCursor;
        private TowerBoxQueue towerBoxQueue;

        private bool rendering;

        private void Awake()
        {
            towerPlacementCursor = GetComponent<TowerPlacementCursor>();
            towerBoxQueue = FindObjectOfType<TowerBoxQueue>();
        }

        public void RenderNextTransparentBox()
        {
            rendering = true;
            
            var towerBox = towerBoxQueue.PreviewNextBox();
            
            if (currentTransparentBox != null)
            {
                Destroy(currentTransparentBox.gameObject);
            }
            
            currentTransparentBox = Instantiate(towerBox, transform);
            currentTransparentBox.SetTransparent();
        }

        public void StopRendering()
        {
            rendering = false;
            Destroy(currentTransparentBox.gameObject);
        }

        private void Update()
        {
            if (!rendering)
            {
                return;
            }
            
            currentTransparentBox.transform.position = towerPlacementCursor.transform.position;
        }
    }
}