using System;
using System.Collections;
using System.Linq;
using Enums;
using UnityEngine;

namespace Player
{
	public class PlayerMovement : MonoBehaviour
	{
		private const float GracePeriod = 0.2f;

		public bool CanJump { get; set; }
		public bool OnGround { get; set; }
		public bool OnGroundLastFrame { get; set; }
		public int RemainingJumps { get; set; }
	
		private const float BaseSpeed = 6f;
		private const float BaseJump = 400f;
		
		private BoxCollider2D boxCollider;
		private Rigidbody2D playerRigidBody;
		private PlayerEffects playerEffects;
		private PlayerStatus playerStatus;

		private void Awake()
		{
			playerEffects = GetComponent<PlayerEffects>();
			playerRigidBody = GetComponent<Rigidbody2D>();
			boxCollider = GetComponent<BoxCollider2D>();
			playerStatus = GetComponent<PlayerStatus>();
		}

		private void Start()
		{
			CanJump = true;
		}

		private void Update()
		{
			playerStatus.Running = (OnGround && Math.Abs(Input.GetAxis("Horizontal")) > 0.1 && Math.Abs(playerRigidBody.velocity.x) > 0.1);

			OnGround = CheckOnGround();
			OnGroundLastFrame = OnGround;
			
			CanJump = OnGround || RemainingJumps > 0;
			
			playerEffects.Animator.SetBool("Jumping", playerRigidBody.velocity.y > 0);

			if (!OnGround && OnGroundLastFrame && CanJump) 
			{
				StopCoroutine(GraceTimer());
				StartCoroutine(GraceTimer());
			} 
			else if (OnGround) 
			{
				StopCoroutine(GraceTimer());
				RemainingJumps = 1;
				CanJump = true;
			}
		}

		public void Stop()
		{
			playerRigidBody.velocity = Vector2.zero;
		}
	
		public void Jump()
		{
			RemainingJumps--;

			playerRigidBody.velocity = new Vector2(playerRigidBody.velocity.x, 0f);
			playerRigidBody.AddRelativeForce(new Vector2(playerRigidBody.velocity.x, BaseJump));
			playerEffects.PlaySfx("jump");
		}
	
		public void MoveInDirection()
		{
			var modifier = 1f;
			if (playerStatus.CheckPowerUpActive(PowerUp.Speed))
			{
				modifier = 1.5f;
			}

			playerRigidBody.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * BaseSpeed * modifier, playerRigidBody.velocity.y);
		}

		#region MovementChecks
		public bool CanMove(Direction dir) 
		{
			var m = dir == Direction.Right ? 1.0f : -1.0f;
			playerEffects.SetFlipX(dir == Direction.Left);

			RaycastHit2D[] hitOut = {};

			hitOut = hitOut.Concat(Physics2D.RaycastAll(boxCollider.transform.position + new Vector3(boxCollider.offset.x, boxCollider.bounds.size.y / 2.0f + boxCollider.offset.y - 0.05f, 0.0f), transform.right * m, boxCollider.bounds.size.x / 2.0f + 0.1f)).ToArray();
			hitOut = hitOut.Concat(Physics2D.RaycastAll(boxCollider.transform.position + new Vector3(boxCollider.offset.x, boxCollider.bounds.size.y / 4.0f + boxCollider.offset.y, 0.0f), transform.right * m, boxCollider.bounds.size.x / 2.0f + 0.1f)).ToArray();
			hitOut = hitOut.Concat(Physics2D.RaycastAll(boxCollider.transform.position + new Vector3(boxCollider.offset.x, boxCollider.offset.y, 0.0f), transform.right * m, boxCollider.bounds.size.x / 2.0f + 0.1f)).ToArray();
			hitOut = hitOut.Concat(Physics2D.RaycastAll(boxCollider.transform.position - new Vector3(boxCollider.offset.x, boxCollider.bounds.size.y / 4.0f - boxCollider.offset.y, 0.0f), transform.right * m, boxCollider.bounds.size.x / 2.0f + 0.1f)).ToArray();
			hitOut = hitOut.Concat(Physics2D.RaycastAll(boxCollider.transform.position - new Vector3(boxCollider.offset.x, boxCollider.bounds.size.y / 2.0f - boxCollider.offset.y - 0.05f, 0.0f), transform.right * m, boxCollider.bounds.size.x / 2.0f + 0.1f)).ToArray();

			foreach (var r in hitOut) 
			{
				if (r.collider.gameObject != gameObject && r.collider.isTrigger == false) 
				{
					return false;
				}
			}

			return true;
		}

		private bool CheckOnGround() 
		{
			RaycastHit2D[] hitOut = {};

			hitOut = hitOut.Concat(Physics2D.RaycastAll(boxCollider.transform.position + new Vector3(boxCollider.bounds.size.x / 2.0f, boxCollider.offset.y, 0.0f), -transform.up, boxCollider.bounds.size.y / 2.0f + 0.1f)).ToArray();
			hitOut = hitOut.Concat(Physics2D.RaycastAll(boxCollider.transform.position + new Vector3(0.0f, boxCollider.offset.y, 0.0f), -transform.up, boxCollider.bounds.size.y / 2.0f + 0.1f)).ToArray();
			hitOut = hitOut.Concat(Physics2D.RaycastAll(boxCollider.transform.position - new Vector3(boxCollider.bounds.size.x / 2.0f, boxCollider.offset.y, 0.0f), -transform.up, boxCollider.bounds.size.y / 2.0f + 0.1f)).ToArray();

			foreach (var r in hitOut) 
			{
				if (r.collider.gameObject != gameObject && r.collider.isTrigger == false) {
				
					return true;
				}
			}

			return false;
		}
	

		#endregion	
	
		private IEnumerator GraceTimer()
		{
			yield return new WaitForSeconds(GracePeriod);
			CanJump = false;
		}
	}
}
