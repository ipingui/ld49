using System;
using UnityEngine;

namespace Player.Pickups
{
    public abstract class Pickup : MonoBehaviour
    {
        public abstract void OnPickup(PlayerStatus playerStatus);

        private void OnTriggerEnter2D(Collider2D other)
        {
            var playerStatus = other.gameObject.GetComponent<PlayerStatus>();
            if (playerStatus)
            {
                OnPickup(playerStatus);
                
                Destroy(gameObject);
            }
        }
    }
}