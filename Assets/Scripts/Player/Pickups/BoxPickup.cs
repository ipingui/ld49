using Sfx;

namespace Player.Pickups
{
    public class BoxPickup : Pickup
    {
        public override void OnPickup(PlayerStatus playerStatus)
        {
            SfxManager.Instance.PlaySfx("pickup");
            playerStatus.GiveBox();
        }
    }
}