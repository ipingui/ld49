using Enums;
using UnityEngine;

namespace Player.Pickups
{
    public class PowerUpPickup : Pickup
    {
        [SerializeField] private PowerUp powerUpType;
        public override void OnPickup(PlayerStatus playerStatus)
        {
            playerStatus.ActivatePowerUp(powerUpType);
        }
    }
}