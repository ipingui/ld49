
using Camera;
using Enums;
using Tower;
using UnityEngine;

namespace Player
{
    public class PlayerInputHandler : MonoBehaviour
    {    
        public bool HasControl { get; set; }
        public bool HasMovementControl { get; private set; }

        private PlayerMovement playerMovement;
        private TowerPlacementCursor towerPlacementCursor;
        private CameraController cameraController;

        private Direction directionLastFrame;
        private float rawHorizontalAxisValueLastFrame;
        private float rawVerticalAxisValueLastFrame;

        private void Awake()
        {
            HasMovementControl = true;
            HasControl = true;
            directionLastFrame = Direction.Right;

            cameraController = FindObjectOfType<CameraController>();
            playerMovement = GetComponent<PlayerMovement>();
            towerPlacementCursor = FindObjectOfType<TowerPlacementCursor>();
        }
        
        private void Update()
        {
            if (!HasControl)
            {
                return;
            }

            if (Input.GetButtonDown("ToggleMode"))
            {
                HasMovementControl = !HasMovementControl;
                if (HasMovementControl)
                {
                    cameraController.ActivateGameplayCamera();
                }
                else
                {
                    cameraController.ActivateBoxPlacementCamera();
                    playerMovement.Stop();
                }
            }
            
            if (HasMovementControl) 
            {
                UpdateMovementInput();
            }
            else
            {
                UpdateCursorInput();
            }
            
            rawHorizontalAxisValueLastFrame = Input.GetAxisRaw("Horizontal");
            rawVerticalAxisValueLastFrame = Input.GetAxisRaw("Vertical");
        }
        
        private void UpdateMovementInput()
        {
            var horizontalAxisValue = Input.GetAxis("Horizontal");

            var direction = directionLastFrame;
            if (horizontalAxisValue > 0.2f) 
            {
                direction = Direction.Right;
            }
            else if (horizontalAxisValue < -0.2f) 
            {
                direction = Direction.Left;
            }

            if (playerMovement.CanMove(direction))
            {
                playerMovement.MoveInDirection();
            }

            if (Input.GetButtonDown("Jump") && 
                (playerMovement.CanJump || playerMovement.RemainingJumps > 0)) 
            {
                playerMovement.Jump();
            }

            directionLastFrame = direction;
        }

        private void UpdateCursorInput()
        {
            if (rawHorizontalAxisValueLastFrame != 0f || rawVerticalAxisValueLastFrame != 0f)
            {
                return; // Stop the value going up way too high.
            }
            
            var horizontalAxisValue = Input.GetAxisRaw("Horizontal");
            if (horizontalAxisValue > 0.2f) 
            {
                towerPlacementCursor.MoveRight();
            }
            else if (horizontalAxisValue < -0.2f) 
            {
                towerPlacementCursor.MoveLeft();
            }

            var verticalAxisValue = Input.GetAxisRaw("Vertical");
            if (verticalAxisValue > 0.2f) 
            {
                towerPlacementCursor.MoveUp();
            }
            else if (verticalAxisValue < -0.2f) 
            {
                towerPlacementCursor.MoveDown();
            }

            if (Input.GetButtonDown("PlaceBox"))
            {
                towerPlacementCursor.PlaceNextBox();
            }
        }
    }
}
