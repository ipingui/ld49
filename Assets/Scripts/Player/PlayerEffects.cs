using System;
using UnityEngine;

namespace Player
{
    public class PlayerEffects : MonoBehaviour
    {
        public Animator Animator { get; private set; }
        private AudioSource audioSource;
        private SpriteRenderer spriteRenderer;

        private void Awake()
        {
            Animator = GetComponent<Animator>();
            audioSource = GetComponent<AudioSource>();
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void PlaySfx(string sfxName)
        {
            var path = $"sfx/{sfxName}";
            var clip = Resources.Load<AudioClip>(path);
            audioSource.PlayOneShot(clip);
        }

        public void SetFlipX(bool flipX)
        {
            spriteRenderer.flipX = flipX;
        }
    }
}