using System;
using Enums;
using UnityEngine;

namespace Player
{
    public class PlayerEnvironmentInteractions : MonoBehaviour
    {
        private PlayerStatus _playerStatus;

        private void Awake()
        {
            _playerStatus = GetComponent<PlayerStatus>();
        }

        public void PickupPowerUp(PowerUp powerUp)
        {
            _playerStatus.ActivatePowerUp(powerUp);
        }

        public void PickupBox()
        {
            _playerStatus.GiveBox();
        }
    }
}