using System;
using System.Collections.Generic;
using Enums;
using UnityEngine;

namespace Player
{
    public class PlayerStatus : MonoBehaviour
    {
        public bool Running { get; set; }
        public int BoxesAvailableToPlace { get; private set; }

        private HashSet<PowerUp> powerUps;
        private PlayerEffects _playerEffects;
        
        private void Awake()
        {
            powerUps = new HashSet<PowerUp>();
            BoxesAvailableToPlace = 0;
            _playerEffects = GetComponent<PlayerEffects>();
        }

        private void Update()
        {
            _playerEffects.Animator.SetBool("Running", Running);
        }

        public bool CheckPowerUpActive(PowerUp powerUp)
        {
            return powerUps.Contains(powerUp);
        }

        public void ActivatePowerUp(PowerUp powerUp)
        {
            powerUps.Add(powerUp);
        }

        public void DeactivatePowerUp(PowerUp powerUp)
        {
            powerUps.Remove(powerUp);
        }

        public void GiveBox()
        {
            BoxesAvailableToPlace += 1;
        }

        public bool UseBox()
        {
            BoxesAvailableToPlace -= 1;
            if (BoxesAvailableToPlace < 0)
            {
                BoxesAvailableToPlace = 0;
                return false;
            }

            return true;
        }

        public bool CanPlaceBox()
        {
            return BoxesAvailableToPlace > 0;
        }
    }
}